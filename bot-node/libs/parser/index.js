const parser = (data = { command: '', content: '' }) => {
  const [command, ...params] = data.command.toLowerCase().split(/\s/);
  return { command, params };
};

module.exports = parser;
