const async = require('async');
const test = require('tape');

const findNumber = require('./find-number');

function getRequest() {
  return {
    header() {
      return 'kikooplop';
    },
  };
}

function getResponse(t, message, cb) {
  return {
    json: (data) => {
      t.equal(data.message, message, `findnumber sent the message: ${message}`);
      cb();
    },
  };
}

test('Command findnumber', (t) => {
  t.plan(6);

  t.equal(typeof findNumber, 'function', 'findnumber is a function');

  const req = getRequest();

  async.waterfall([
    (cb) => {
      const nextRes = getResponse(t, 'Choose a number between 0 and 100. I\'ll try to find it... Is it 50 ? Type «findnumber less», «findnumber more» or «findnumber yep»!', cb);
      findNumber(req, nextRes);
    },
    (cb) => {
      const nextRes = getResponse(t, 'Is it 25 ? Type «findnumber less», «findnumber more» or «findnumber yep»!', cb);
      findNumber(req, nextRes, {
        command: 'findnumber',
        params: ['less'],
      });
    },
    (cb) => {
      const nextRes = getResponse(t, 'Is it 37 ? Type «findnumber less», «findnumber more» or «findnumber yep»!', cb);
      findNumber(req, nextRes, {
        command: 'findnumber',
        params: ['more'],
      });
    },
    (cb) => {
      const nextRes = getResponse(t, 'Cool! See you!', cb);
      findNumber(req, nextRes, {
        command: 'findnumber',
        params: ['yep'],
      });
    },
    (cb) => {
      const nextRes = getResponse(t, 'Choose a number between 0 and 100. I\'ll try to find it... Is it 50 ? Type «findnumber less», «findnumber more» or «findnumber yep»!', cb);
      findNumber(req, nextRes);
    },
  ]);
});
