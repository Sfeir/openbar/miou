const findNumber = require('./find-number');
const helloWorld = require('./hello-world');
const pong = require('./pong');
const aboutSfeir = require('./about-sfeir');
const help = require('./help');
const testtable = require('./testtable');
const testpie = require('./testpie');
const testbar = require('./testbar');


module.exports = {
  findNumber,
  helloWorld,
  pong,
  aboutSfeir,
  help,
  testtable,
  testpie,
  testbar
};
