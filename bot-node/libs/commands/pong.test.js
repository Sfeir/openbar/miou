const sinon = require('sinon');
const test = require('tape');

const pong = require('./pong');

test('Command pong', (t) => {
  t.plan(2);

  t.equal(typeof pong, 'function', 'pong is a function');

  const res = {
    json: sinon.spy(),
  };

  pong({}, res);

  t.equal(res.json.calledWith({ message: 'pong' }), true, 'pong send the «pong» message');
});
