const pong = (req, res) => {
  res.json({
    message: 'pong',
    template: {
      type: "default",
      title: "Table tennis",
      subtitle: "Individual sport",
      content:"Table tennis, also known as ping pong, is a sport in which two or four players hit a lightweight ball back and forth across a table using a small bat. The game takes place on a hard table divided by a net. Except for the initial serve, the rules are generally as follows: players must allow a ball played toward them to bounce one time on their side of the table, and must return it so that it bounces on the opposite side at least once." ,
      img: "https://upload.wikimedia.org/wikipedia/commons/thumb/5/58/Mondial_Ping_-_Men%27s_Singles_-_Round_4_-_Kenta_Matsudaira-Vladimir_Samsonov_-_57.jpg/1280px-Mondial_Ping_-_Men%27s_Singles_-_Round_4_-_Kenta_Matsudaira-Vladimir_Samsonov_-_57.jpg",
      controls: [
        {
          label: "Wikipedia",
          action: "link",
          url: "https://en.wikipedia.org/wiki/Table_tennis"
        },
        {
          label: "ITTF",
          action: "link",
          url: "https://www.ittf.com/"
        }
      ]
    }
    // ,
    // suggestions: [
    //   {
    //     label: "Learn more",
    //     action: "/pingpong"
    //   },
    //   {
    //     label: "About SFEIR Assistant",
    //     action: "/pingpong"
    //   }
    // ]
  });
};

module.exports = pong;
