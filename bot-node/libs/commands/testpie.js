const testpie = (req, res) => {
    res.json({
      message: 'Sending piechart',
      template: {
        type: "pie",
        title: "Number of letters in the word Doggy",
        content: {
          series: [1, 1, 2, 1],
          percent: true
        }
      }
    });
  };
  
  module.exports = testpie;
  