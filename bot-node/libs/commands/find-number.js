const { MIOU_SESSION_ID } = require('../constants');

const players = {};

function reset(id) {
  players[id] = {
    min: 0,
    current: 50,
    max: 100,
  };
}

function getNextCurrent(currentPlayer) {
  return Math.floor(currentPlayer.min + ((currentPlayer.max - currentPlayer.min) / 2));
}

const findNumber = (req, res, body = {}) => {
  const [keyword] = body.params || [];
  
  const playerId = req.header(MIOU_SESSION_ID);
  if (!players[playerId]) {
    reset(playerId);
  }
  
  let message = '';
  let controls = [
    {
      label: "Smaller",
      action: "command",
      command: "/findnumber less"
    },
    {
      label: "Bigger",
      action: "command",
      command: "/findnumber more"
    },
    {
      label: "That's it!",
      action: "command",
      command: "/findnumber yep"
    }
  ];
  if (keyword === 'less') {
    players[playerId].max = players[playerId].current;
    players[playerId].current = getNextCurrent(players[playerId]);
    message = `Is it ${players[playerId].current} ? Type «findnumber less», «findnumber more» or «findnumber yep»!`;
  } else if (keyword === 'more') {
    players[playerId].min = players[playerId].current;
    players[playerId].current = getNextCurrent(players[playerId]);
    message = `Is it ${players[playerId].current} ? Type «findnumber less», «findnumber more» or «findnumber yep»!`;
  } else if (keyword === 'yep') {
    reset(playerId);
    controls = null;
    message = 'Cool! See you!';
  } else {
    message = `Choose a number between ${players[playerId].min} and ${players[playerId].max}. I'll try to find it... Is it ${players[playerId].current} ? Type «findnumber less», «findnumber more» or «findnumber yep»!`;
  }
  
  res.json({
    message,
    debug: {
      id: playerId,
      min: players[playerId].min,
      current: players[playerId].current,
      max: players[playerId].max,
    },
    controls
  });
};

module.exports = findNumber;
