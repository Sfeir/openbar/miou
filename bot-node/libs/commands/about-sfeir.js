const aboutSfeir = (req, res) => {
  res.json({
    message: 'SFEIR Assistant is a modular bot system. It allows you to code your own bot behavior without having to think about client and server logic.',
    controls: [
      {
        label: "Help",
        action: "command",
        command: "/help"
      }
    ]
  });
};

module.exports = aboutSfeir;
