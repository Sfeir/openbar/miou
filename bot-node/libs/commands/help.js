const help = (req, res) => {
  res.json({
    message: "Start by typing commands or by clicking suggestions. Try typing '/findnumber', '/ping' or '/aboutsfeir'.",
    controls: [
      {label: "Find Number", action: "command", command: "/findnumber"},
      {label: "Ping Pong", action: "command", command: "/ping"},
      {label: "About SFEIR Assistant", action: "command", command: "/aboutsfeir"},
      {label: "Test table", action: "command", command: "/testtable"},
      {label: "Test pie chart", action: "command", command: "/testpie"},
      {label: "Test bar chart", action: "command", command: "/testbar"}
    ]
  
  });
};

module.exports = help;
