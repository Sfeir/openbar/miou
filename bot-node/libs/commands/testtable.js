const testtable = (req, res) => {
    res.json({
      message: 'Sending table',
      template: {
        type: "table",
        content: [
          ["Data", "Fake data", "rectant data"],
          ["good data", "bad data", "opulent data"],
          ["small Data", "big data", "just data"]
        ]
      }
    });
  };
  
  module.exports = testtable;
  