const helloWorld = (req, res) => {
  res.json({
    message: 'Hello, world!',
  });
};

module.exports = helloWorld;
