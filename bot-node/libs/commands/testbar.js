const testbar = (req, res) => {
    res.json({
      message: 'Sending bar chart',
      template: {
        type: "line",
        title: "Number of letters in the word Doggy",
        content: {
          labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
          series: [
            [12, 9, 7, 8, 5],
            [2, 1, 3.5, 7, 3],
            [1, 3, 4, 5, 6]
          ]
        }
      }
    });
  };
  
  module.exports = testbar;
  