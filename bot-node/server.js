const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');

const libs = require('./libs/commands/index');
const parse = require('./libs/parser');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(helmet());

const port = process.env.PORT || 8081;

app.post('/', (req, res) => {
  const body = parse(req.body);

  const commands = {
    '/hello': 'helloWorld',
    '/ping': 'pong',
    '/findnumber': 'findNumber',
    '/aboutsfeir': 'aboutSfeir',
    '/help': 'help',
    '/testtable': 'testtable',
    '/testpie': 'testpie',
    '/testbar': 'testbar'
  };

  if (commands[body.command] && typeof libs[commands[body.command]] === 'function') {
    libs[commands[body.command]](req, res, body);
  } else {
    res.status(404).json({
      message: 'Command not found',
    });
  }
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`);
});
