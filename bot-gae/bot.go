package bot

import (
	"encoding/json"
	"fmt"
	"net/http"

	"google.golang.org/appengine"
	"google.golang.org/appengine/log"

	"gitlab.com/Sfeir/openbar/miou/bot-gae/commands"
	"gitlab.com/Sfeir/openbar/miou/bot-gae/common"
)

func init() {
	// Declare route handlers.
	http.HandleFunc("/", handler)
}

func handler(w http.ResponseWriter, r *http.Request) {
	// Get AppEngine context.
	ctx := appengine.NewContext(r)

	// Decode received message.
	m := common.Message{}
	err := json.NewDecoder(r.Body).Decode(&m)
	if err != nil {
		w.WriteHeader(400)
		fmt.Fprintf(w, "%s", err)
		return
	}

	log.Debugf(ctx, "Received message : "+fmt.Sprintf("%+v\n", m))

	// Get command handler.
	h, ok := commands.Handlers[m.Command]
	if !ok {
		w.WriteHeader(400)
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, `{"message": "Command not found"}`);
	} else {
		h(ctx, w, &m)
	}
}
