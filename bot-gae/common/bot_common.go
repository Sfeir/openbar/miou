package common

type Message struct {
	Command string `json:"command"`
	Content string `json:"content"`
}
