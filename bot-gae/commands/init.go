package commands

import (
	"fmt"
	"net/http"

	"golang.org/x/net/context"
	"google.golang.org/appengine/log"

	"gitlab.com/Sfeir/openbar/miou/bot-gae/common"
)

func initConnection(ctx context.Context, w http.ResponseWriter, msg *common.Message) {
	log.Debugf(ctx, "Sending initialization data")

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	fmt.Fprint(w, `{
		"message": "Welcome to Miou",
		"controls": [
			{
				"label": "Help",
				"action": "command",
				"target": "/help"
			}
		]
		
    }`)
}
