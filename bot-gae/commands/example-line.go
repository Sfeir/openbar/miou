package commands

import (
	"fmt"
	"net/http"

	"golang.org/x/net/context"
	"google.golang.org/appengine/log"

	"gitlab.com/Sfeir/openbar/miou/bot-gae/common"
)

func exampleLine(ctx context.Context, w http.ResponseWriter, msg *common.Message) {
	log.Debugf(ctx, "Test line command")

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	fmt.Fprint(w, `{
		"message": "example line chart",
		"template": {
			"type": "line",
			"title": "Number of letters in the word Doggy",
			"content": {
			  "labels": ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"],
			  "series": [
				[12, 9, 7, 8, 5],
				[2, 1, 3.5, 7, 3],
				[1, 3, 4, 5, 6]
			  ]
			}
		  }
    }`)
}
