package commands

import (
	"fmt"
	"net/http"

	"golang.org/x/net/context"
	"google.golang.org/appengine/log"

	"gitlab.com/Sfeir/openbar/miou/bot-gae/common"
)

func exampleTable(ctx context.Context, w http.ResponseWriter, msg *common.Message) {
	log.Debugf(ctx, "example table command")

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	fmt.Fprint(w, `{
		"message": "Sending table",
		"template": {
			"type": "table",
			"content": [
				["City", "Hotel prices", "Restaurant prices"],
				["Paris", "92€", "20€"],
				["New-York", "216€", "24€"],
				["London", "147€", "21€"]
			]
		}
    }`)
}
