package commands

import (
	"net/http"

	"golang.org/x/net/context"

	"gitlab.com/Sfeir/openbar/miou/bot-gae/common"
)

var Handlers = map[string]func(context.Context, http.ResponseWriter, *common.Message){
	"/hello": hello,
	"/ping":  pong,
	"/help":  help,
	"/examplepie": examplePie,
	"/exampleline": exampleLine,
	"/exampletable": exampleTable,
	"$INIT": initConnection,
}
