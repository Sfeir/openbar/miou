package commands

import (
	"fmt"
	"net/http"

	"golang.org/x/net/context"
	"google.golang.org/appengine/log"

	"gitlab.com/Sfeir/openbar/miou/bot-gae/common"
)

func examplePie(ctx context.Context, w http.ResponseWriter, msg *common.Message) {
	log.Debugf(ctx, "Test pie command")

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	fmt.Fprint(w, `{
		"message": "example pie chart",
		"template": {
			"type": "pie",
			"title": "Number of letters in the word Doggy",
			"content": {
				"label": ["D", "O", "G", "Y"],
				"series": [1, 1, 2, 1],
				"percent": true
			}
		  }
    }`)
}
