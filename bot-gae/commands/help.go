package commands

import (
	"fmt"
	"net/http"

	"golang.org/x/net/context"
	"google.golang.org/appengine/log"

	"gitlab.com/Sfeir/openbar/miou/bot-gae/common"
)

func help(ctx context.Context, w http.ResponseWriter, msg *common.Message) {
	log.Debugf(ctx, "Help command")

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	fmt.Fprint(w, `{
		"message": "Start by typing commands or by clicking suggestions.",
		"controls": [
			{"label": "Ping Pong", "action": "command", "target": "/ping"},
			{"label": "example table", "action": "command", "target": "/exampletable"},
			{"label": "example pie chart", "action": "command", "target": "/examplepie"},
			{"label": "example line chart", "action": "command", "target": "/exampleline"}
		]
		
    }`)
}
