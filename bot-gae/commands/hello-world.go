package commands

import (
	"net/http"
	"fmt"

	"golang.org/x/net/context"
	"google.golang.org/appengine/log"

	"gitlab.com/Sfeir/openbar/miou/bot-gae/common"
)

func hello(ctx context.Context, w http.ResponseWriter, msg *common.Message) {
	log.Debugf(ctx, "Hello world !!")

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	fmt.Fprint(w, `{"message": "Hello world from Google App Engine !!"}`)
}
