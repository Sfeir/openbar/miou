# miou

:construction: WIP :construction:

> «miou» is like «meow» in french. Like «miaou», but different. Also, [mioumi. ou.](https://www.youtube.com/watch?v=GsEpAx5-324) 

## client

To build the things
```bash
npm i
npx flow
npm run build

# Launch locally in AppEngine emulator.
dev_appserver.py app.yaml

# Deploy to AppEngine.
gcloud app deploy --quiet --version=v1
```

## server

```bash
# Install dependencies.
go get google.golang.org/appengine
go get github.com/satori/go.uuid

# Launch locally in AppEngine emulator.
dev_appserver.py app.yaml

# Deploy to AppEngine.
gcloud app deploy --quiet --version=v1

```

## bot

### bot-node

```bash
npm i
npm start
```

To launch tests (only lint for now  :sweat_smile:):

```bash
npm test
```

### bot-rs

```bash
cargo run
```

### bot-gae

```bash
# Install dependencies.
go get google.golang.org/appengine

# Launch locally in AppEngine emulator.
dev_appserver.py app.yaml

# Deploy to AppEngine.
gcloud app deploy --quiet --version=v1

```


# Documentation

## [Introduction](https://gitlab.com/Sfeir/openbar/miou/wikis/1-Introduction)


## [Request format](https://gitlab.com/Sfeir/openbar/miou/wikis/2-Request-format)


## [Response format](https://gitlab.com/Sfeir/openbar/miou/wikis/3-response-format)


## [$INIT command](https://gitlab.com/Sfeir/openbar/miou/wikis/6-initialization)