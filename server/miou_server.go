package miou

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"golang.org/x/net/context"
	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/urlfetch"

	"github.com/satori/go.uuid"

	BotConfig "gitlab.com/Sfeir/openbar/miou/server/bot"
)

const (
	MIOU_SESSION_ID string = "Miou-Session-Id"
)

type Message struct {
	Command string `json:"command"`
	Content string `json:"content"`
}

func init() {
	// Declare routes.
	http.HandleFunc("/msg", MessageHandler)
	http.HandleFunc("/init", MessageHandler)
}

func MessageHandler(w http.ResponseWriter, r *http.Request) {
	// Get AppEngine context.
	ctx := appengine.NewContext(r)

	// Set response headers.
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type, "+MIOU_SESSION_ID)

	if r.Method == http.MethodOptions {
		w.WriteHeader(200)
		fmt.Fprintf(w, "OK")
	} else if r.Method == http.MethodPost {
		// Decode received message.
		m := Message{}
		err := json.NewDecoder(r.Body).Decode(&m)
		if err != nil {
			w.WriteHeader(400)
			fmt.Fprintf(w, "%s", err)
			return
		}

		// Get session ID.
		var miouSessionId string = getSessionId(r)

		log.Debugf(ctx, "POST /msg : "+fmt.Sprintf("%+v\n", m))

		// Send message to bot and get response.
		botData, status, err := sendToBot(ctx, m, miouSessionId)

		w.Header().Set("Content-Type", "application/json")
		w.Header().Set(MIOU_SESSION_ID, miouSessionId)
		w.WriteHeader(status)

		if err != nil {
			fmt.Fprintf(w, "%s", err)
		} else {
			fmt.Fprintf(w, "%s", botData)
		}
	} else {
		http.NotFound(w, r)
	}
}

func getSessionId(r *http.Request) string {
	miouSessionId := r.Header.Get(MIOU_SESSION_ID)

	if miouSessionId == "" {
		miouSessionId = uuid.NewV4().String()
	}

	return miouSessionId
}

func sendToBot(ctx context.Context, data Message, sessionId string) (string, int, error) {
	log.Debugf(ctx, "Calling bot : url=" + BotConfig.BOT_URL)

	jsonStr, err := json.Marshal(data)
	if err != nil {
		return "", 400, errors.New("Failed to read request data")
	}
	req, err := http.NewRequest("POST", BotConfig.BOT_URL, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := urlfetch.Client(ctx)
	resp, err := client.Do(req)
	if err != nil {
		return "", 500, errors.New("Failed to create request to bot")
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", 500, errors.New("Failed to read bot response")
	}

	return string(body), resp.StatusCode, nil
}
