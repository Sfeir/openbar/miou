
{

    let id = 0;

    function getNewId() {
        return id++;
    }

    function scroll(el) {
        el.scrollTop = el.scrollHeight;
    }

    function scrollTo(pos) {
        mainWindow.scrollTop = pos;
    }

    function getOffset(el) {
        var _y = 0;
        while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
            _y += el.offsetTop - el.scrollTop;
            el = el.offsetParent;
        }
        return _y;
    }

    function getLastOfClass(string) {
        const tmp = document.getElementsByClassName(string);
        return tmp.length ? tmp[tmp.length - 1] : 0;
    }

    function initElements() {
        btn.addEventListener("touchend", e => onSubmit(e), false);
        btn.addEventListener('click', e => onSubmit(e));
        form_helpButton.addEventListener("mouseup", e => askServer("/help"));
    }
    var sum = function (a, b) {
        return a + b;
    };
}