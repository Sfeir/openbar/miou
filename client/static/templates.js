
//Templates

const template_tim = `
    <div class="box">
    <div class="box_header clearfix">
    <div class="box_header_texts">
    <div class="box_header_title">
    {{title}}
    </div>
    <div class="box_header_subtitle">
    {{subtitle}}
    </div>
    </div>
    {{img}}
    </div>
    <div class="box_content">
    {{content}}
    </div>
    {{controlsWrapper}}
    </div>
    `;
const template_img = `<img class="box_header_figure" src="{{img}}"/>`;
const template_robot = "<img src='bot.svg' alt='SFEIR Bot' class='botIcon'>";

const template_spinner = `
    <div class="spinner">
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
    </div>
    `;

const SPINNER = null;