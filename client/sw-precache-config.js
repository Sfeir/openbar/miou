module.exports = {
  staticFileGlobs: ['static/**/*.{js,html,css,png,jpg,gif}'],
  stripPrefix: 'static',
}