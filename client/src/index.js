// @flow

{
    initElements();
    initConnection();
    //==================
    //Functions that are bound to an event listener
    function onSubmit(e: Event): void {
        e.preventDefault();
        const val: string = text.value;
        askServer(val);
        text.value = '';
    } 

    function createBaseElements (val, isInit) {
        const messageGroup: HTMLDivElement = document.createElement('div');
        messageGroup.classList.add("messageGroup");
        if (!isInit) {messageGroup.append(createMessageBubble(val, 'right'));}
        responseMessage = createMessageBubble(SPINNER, isInit?'welcome':'left');
        messageGroup.append(responseMessage);
        messages.append(messageGroup);
        return {responseMessage: responseMessage, messageGroup: messageGroup};
    }

    function replaceSpinnerWithContent(data, response) {
        let bubble = response.responseMessage.getElementsByClassName('message_bubble')[0];
        bubble.innerHTML = "";
        bubble.append(document.createTextNode(data.message));
        createAdditionalContent(data.template, response.messageGroup);
        response.messageGroup.append(createInteractiveElements(data.controls,"suggestion"));
        scrollTo(getOffset(getLastOfClass("message--left")));
    }
    
    function initConnection(): void {
        const response = createBaseElements("val", true);
        fetch(MIOU_SERVER_URL + "/init", {
            method: "POST",
            body: JSON.stringify({command: "$INIT"}),
            headers: {
                [MIOU_SESSION_ID]: sessionStorage.getItem(MIOU_SESSION_ID) || undefined
            }
        })
        .then( res => {
            if (res.headers.has(MIOU_SESSION_ID)) {
                sessionStorage.setItem(MIOU_SESSION_ID, res.headers.get(MIOU_SESSION_ID));
            }
            return res.status===200?res.json():{message: "Welcome to Miou"};
        })
        .then(data => {
            replaceSpinnerWithContent(data, response);
        })
        .catch(err => console.log(err));
    }



    //==================
    //Fetch data, call layout functions
    function askServer(raw: string): void {
        let val = raw.trim().toLowerCase();
        text.focus();
        if (val) {
            if (!val.startsWith('/')) {val = "/"+val;}
            const response = createBaseElements(val, false);
            scroll(mainWindow);
            fetch(MIOU_SERVER_URL + "/msg", {
                method: "POST",
                body: JSON.stringify({command: val}),
                headers: {
                    [MIOU_SESSION_ID]: sessionStorage.getItem(MIOU_SESSION_ID) || undefined
                }
            })
            .then((res) => {
                if (res.headers.has(MIOU_SESSION_ID)) {
                    sessionStorage.setItem(MIOU_SESSION_ID, res.headers.get(MIOU_SESSION_ID));
                }
                return res.status===200?res.json():{message: "Command not found."};
            })
            .then((data) => {
                replaceSpinnerWithContent(data, response);
               
            })
            .catch(err => {
                console.log(err);
            });  
        }
    }
    
    
    //==================
    //Write main messages
    function createMessageBubble(val: string, alignment: string): HTMLDivElement {
        const wrapper: HTMLDivElement = document.createElement('div');
        const bubble: HTMLDivElement = document.createElement('div');
        bubble.classList.add("message_bubble");
        let content;
        if (alignment === "left") {
            content = document.createElement('div');
            if (val != SPINNER) {
                content.append(val);
            } else {
                content.innerHTML += template_spinner;
            }
        } else {
            content = document.createTextNode(val)
        }
        bubble.append(content);
        wrapper.classList.add("message", `message--${alignment}`, "clearfix");
        if (alignment !== "right") {
            wrapper.innerHTML += template_robot;
        }
        wrapper.append(bubble);
        return wrapper;
    }
    
    
    
    //==================
    //Calls for a templated element and populates it if necessary, thanks to an anchor.
    function createAdditionalContent(template, messageGroup) {
        const response = createTemplatedElement(template);
        messageGroup.append(response.div);
        if (template) {
            switch (template.type) {
                case "pie": {
                    const labelFuncObj = {
                        labelInterpolationFnc: function(value, idx) {
                            let percentage = template.content.percent===true?(Math.round(value / template.content.series.reduce(sum) * 100) + '%'):value;
                            let label = template.content.label?template.content.label[idx] + ': ':"";
                            return label + percentage;
                        }
                    }
                    document.getElementById("a"+response.id).classList.add("ct-major-third", "chart");
                    new Chartist.Pie("#a"+response.id, template.content, labelFuncObj);
                    break;
                }
                case "line": {
                    document.getElementById("a"+response.id).classList.add("ct-major-third", "chart");
                    new Chartist.Line("#a"+response.id, template.content);
                    break;
                }
                default: {
                    break;
                }
            }
        }
    }
    
    
    
    //==================
    //Create the additional templated element
    function createTemplatedElement(template) {
        const wrapper = document.createElement("div");
        if (template) {
            switch (template.type) {
                case "default": {
                    return {div: createDefaultElement(template), id: null};
                }
                case "table": {
                    return {div: createTable(template.content), id: null};
                }
                default: {
                    let id = getNewId();                    
                    return {div: createDefaultElement(template, "<div id='a"+id+"'></div>"), id: id};
                }
            }
        }
        return {div: wrapper, id: null};
    }
    
    
    
    //==================
    //Create additional content: Table
    function createTable(tabularData): HTMLDivElement {
        const wrapper: HTMLDivElement = document.createElement("table");
        wrapper.classList.add("box", "table");
        tabularData.forEach((line, index) => {
            let htmlline = document.createElement("tr");
            wrapper.append(htmlline);
            line.forEach(cell => {
                let htmlcell = document.createElement(index?"td":"th");
                htmlcell.innerHTML = cell;
                htmlline.append(htmlcell);
            });
        });
        return wrapper;
    }
    
    //==================
    //Create additional content: Default template
    function createDefaultElement(data, contentPlaceholder?): HTMLDivElement {
        const wrapper : HTMLDivElement = document.createElement('div');
        if (data){
            let timData = {
                title: "<div></div>",
                subtitle: "<div></div>",
                content: "<div></div>",
                controlsWrapper: "<div class='controlsWrapper'></div>",
                img: "<div></div>"
            }
            if (!contentPlaceholder) {contentPlaceholder = data.content;}
            //Use the placeholder if it exists, if not use the response.
            if (data.img) {data.img = tim(template_img, data)}
            timData = { ...timData, ...data, content: contentPlaceholder};
            wrapper.innerHTML += tim(template_tim, timData);
            if (data.controls) {
                const controlsDiv: HTMLDivElement = wrapper.getElementsByClassName("controlsWrapper")[0];
                controlsDiv.classList.add("box_content");
                controlsDiv.append(createInteractiveElements(data.controls, "btn"));
            }
        }
        return wrapper;
    }
    
    //==================
    //Create additional content: Button array
    //TODO: Custom types in babel ? ControlData array
    function createInteractiveElements(elements,elementClass: string): HTMLDivElement {
        const wrapper : HTMLDivElement = document.createElement('div');
        if (elements) {
            elements.forEach(element => {
                const tmpControl = document.createElement("button");
                tmpControl.classList.add(elementClass)
                //TODO Manage to multiline without messing up the tabs
                tmpControl.addEventListener("mouseup",element.action==="command"?()=> askServer(element.target):()=> window.open(element.target, '_blank'));
                tmpControl.append(document.createTextNode(element.label));
                wrapper.append(tmpControl);
            });
        }
        return wrapper;
    }
    
    
    // Uncomment this to add some messages.
    //
    // const msgInterval = setInterval(() => {
    //     messages.append(createElement(Math.random().toString(36).substring(2), 'left'));
    //     messages.append(createElement(Math.random().toString(36).substring(2), 'right'));
    //     scroll(messages);
    // }, 100);
    //
    // setTimeout(() => {
    //     clearInterval(msgInterval);
    // }, 500);
}