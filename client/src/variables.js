// @flow


const $ = document.querySelector.bind(document);
const messages: HTMLDivElement = $('#messages');
const mainWindow: HTMLDivElement = $('#mainWindow');
const btn: HTMLButtonElement = $('#form_sendButton');
const text: HTMLInputElement = $('#form_input');
const welcomeButton_about: HTMLInputElement = $('#welcomeButton_about');
const welcomeButton_help: HTMLInputElement = $('#welcomeButton_help');
const form_helpButton: HTMLInputElement = $('#form_helpButton');
const MIOU_SESSION_ID: string = 'Miou-Session-Id';
const MIOU_SERVER_URL: string = "MIOU_SERV_PLACEHOLDER";
