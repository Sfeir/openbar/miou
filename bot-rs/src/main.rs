extern crate iron;
extern crate params;

use iron::prelude::*;
use iron::headers::ContentType;
use params::{Params, Value};

fn handle(req: &mut Request) -> IronResult<Response> {
    let mut res = Response::new();
    res.headers.set(ContentType::json());

    match req.get_ref::<Params>().unwrap().find(&["content"]) {
        Some(&Value::String(ref content)) if content == "ping" => {
            res.body = Some(Box::new("{\"message\": \"pong\"}"));
            Ok(res)
        },
        _ => Ok(Response::with(iron::status::NotFound)),
    }
}

fn main() {
    Iron::new(handle).http("localhost:8081").unwrap();
}
